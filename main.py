# -*- coding: utf-8 -*-

from pprint import pprint

def load_cookbook():
    """Чтение кулинарной книги из файла"""
    with open("cook_book.txt") as f:
        state = 1
        # state - состояние конечного автомата:
        # 0 - ожидание пустой строки 
        # 1 - ожидание названия блюда
        # 2 - ожидание количества индигриентов
        # 3 - ожидание описания индигриента
        cook_book = {} # Структура для хранения кулинарной книги
        for line_no, line in enumerate(f, start = 1):
            if not state: # state == 0 ожидание пустой строки
                if line.strip():
                    print("Ошибка в формате кулинарной книги в строке {}."
                          .format(line_no))
                    print("Ожидалась пустая строка.")
                    return None
                state = 1
            elif state == 1:
                line = line.strip()
                if not line:
                    continue # Пусть будет сколько угодно пустых
                             # строк перед описанием
                key = line
                if key in cook_book:
                    print("Ошибка в формате кулинарной книги в строке {}."
                          .format(line_no))
                    print("Повторяется описание блюда.")
                    return None
                cook_book[key] = []
                state = 2
            elif state == 2:
                try:
                    count = int(line)
                except ValueError:
                    print("Ошибка в формате кулинарной книги в строке {}."
                          .format(line_no))
                    print("Ожидалось количество индигриентов.")
                    return None
                item = 0
                state = 3
            elif state == 3:
                try:
                    values = line.split("|")
                    if len(values) != 3:
                        raise ValueError()
                    for i in range(3):
                        values[i] = values[i].strip()
                        if not values[i]:
                            raise ValueError()
                    cook_book[key].append({
                            "ingridient_name": values[0],
                            "quantity": values[1],
                            "measure": values[2]
                            })
                except ValueError:
                    print("Ошибка в формате кулинарной книги в строке {}."
                          .format(line_no))
                    print("Ошибка в описании индигриента.")
                    return None
                item += 1
                if item == count:
                    state = 0
            else:
                print("Ошибка в работе конечного автомата")
                print("Неизвестное состояние: {}.".format(state))
                return None
        return cook_book
    
def get_shop_list_by_dishes(dishes, person_count):
    """
    Расчет количества индигриентов по списку блюд (dishes)
    и количеству персон (person_count)
    """
    cook_book = load_cookbook()
    if cook_book is None:
        print("Поваренная книга пуста или имеет неподдерживаемый формат")
        return None
    ingridients = {}
    for dish in dishes:
        if dish not in cook_book:
            print("Блюдо {} отсутствет в поваренной книге!"
                  .format(dish))
            return None
        for item in cook_book[dish]:
            name = item["ingridient_name"]
            measure = item["measure"]
            try:
                quantity = float(item["quantity"]) * person_count
            except ValueError:
                print("Ошибка. В поваренной книге некорректно задано "
                      "количество ({}) для блюда [{}] ингридиента [{}]"
                      .format(item["quantity"], dish, name))
            if name in ingridients:
                if ingridients[name]["measure"] != measure:
                    print("Ошибка. Программа не умеет переводить "
                          "единицы измерения. Задана единица [{}] "
                          "для ингридиента [{}] блюда [{}]."
                          " Но ранее уже встречалась единица [{}]"
                          " для такого же ингридиента!"
                          .format(measure, name, dish,
                                  ingridients[name]["measure"]))
                    return None
                quantity += ingridients[name]["quantity"]
            ingridients[name] = {
                    "measure": measure,
                    "quantity": quantity
                    }
    return ingridients

def do_tasks():
    "Выполнение задач"
    while True:
        print("\nВведите номер задачи:")
        print("1 - Вывести содержимое кулинарной книги в виде словаря")
        print("2 - Вывести калькуляцию индигриентов для указанного"
              " количества персон")
        print("q - Выход")
        user_input = input(":")
        if user_input == "1":
            print("\nВывод содержимого поваренной книги\n")
            pprint(load_cookbook())
        elif user_input == "2":
            print("\nРасчет ингридиентов для Омлета и Запеченного картофеля"
                  " на 2 персоны\n")
            pprint(get_shop_list_by_dishes(
                    ['Запеченный картофель', 'Омлет'],
                    2))
        elif user_input == "q":
            break;
        else:
            print("Неизвестная задача")
    print()
    
do_tasks()
